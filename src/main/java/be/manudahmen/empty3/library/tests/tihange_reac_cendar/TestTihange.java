package be.manudahmen.empty3.library.tests.tihange_reac_cendar;

import be.manudahmen.empty3.Point3D;
import be.manudahmen.empty3.core.testing.TestObjetSub;

public class TestTihange extends TestObjetSub {
    public static void main(String... args) {
        TestTihange testTihange = new TestTihange();
        testTihange.setMaxFrames(1);
        testTihange.setGenerate(GENERATE_MODEL);
        new Thread(testTihange).start();
    }

    public void ginit() {
        scene().cameraActive(new be.manudahmen.empty3.Camera(new Point3D(4, 0, 1),
                new Point3D(0, 0, 1)));
        scene().add(new Tihange());
    }

    @Override
    public void testScene() throws Exception {
    }
}
