package be.manudahmen.empty3.library.tests.tihange_reac_cendar;

import be.manudahmen.empty3.ColorTexture;
import be.manudahmen.empty3.Point3D;
import be.manudahmen.empty3.RepresentableConteneur;
import be.manudahmen.empty3.core.nurbs.ParametricSurface;
import javafx.scene.Camera;

import java.awt.*;

/**
 * Created by Win on 22-12-18.
 */
public class Tihange extends RepresentableConteneur {
    public Tihange() {
        // Extérieur haut
        ParametricSurface surface = new ParametricSurface() {

            @Override
            public Point3D calculerPoint3D(double u, double v) {
                texture(new ColorTexture(Color.BLACK));
                double x = u - 0.5;
                double y = v * 2;
                double coordLarge = 4 * Math.PI * (x * x + 1);
                return new Point3D(Math.cos(coordLarge), Math.sin(coordLarge), v * 2);
            }
        };
        // Intérieur haut
        ParametricSurface surface1 = new ParametricSurface() {
            @Override
            public Point3D calculerPoint3D(double u, double v) {
                texture(new ColorTexture(Color.BLACK));
                double x = u - 0.5;
                double y = v * 2;
                double coordLarge = 4 * Math.PI * (0.6 - x * x);
                return new Point3D(Math.cos(coordLarge), Math.sin(coordLarge), v * 2);
            }
        };
        // Surface sommet haut
        ParametricSurface surface2 = new ParametricSurface() {
            @Override
            public Point3D calculerPoint3D(double u, double v) {
                texture(new ColorTexture(Color.BLACK));
                double x = u - 0.5;
                double y = 2;
                double coordLarge = 4 * Math.PI * ((1 - x * x) + u * (x * x + 1 + 0.6 - x * x));
                return new Point3D(Math.cos(coordLarge), Math.sin(coordLarge), 1);
            }
        };
        // Surface intérieur bas horizontale
        ParametricSurface surface3 = new ParametricSurface() {
            @Override
            public Point3D calculerPoint3D(double u, double v) {
                texture(new ColorTexture(Color.BLACK));
                double x = u - 0.5;
                double y = 0;
                double coordLarge = 4 * Math.PI * x;
                return new Point3D(Math.cos(coordLarge), Math.sin(coordLarge), y);
            }
        };
        // Surface extérieure bas horizontale
        ParametricSurface surface4 = new ParametricSurface() {

            @Override
            public Point3D calculerPoint3D(double u, double v) {
                texture(new ColorTexture(Color.BLACK));
                double x = u - 0.5;
                double y = -0.1;
                double coordLarge = 4 * Math.PI * x;
                return new Point3D(Math.cos(coordLarge), Math.sin(coordLarge), y);
            }
        };

        // Surface extérieure bas méridionale
        ParametricSurface surface5 = new ParametricSurface() {
            @Override
            public Point3D calculerPoint3D(double u, double v) {
                texture(new ColorTexture(Color.BLACK));
                double x = u - 0.5;
                double y = -0.1 + (v / 10.0);
                double coordLarge = 4 * Math.PI * 1;
                return new Point3D(Math.cos(coordLarge), Math.sin(coordLarge), y);
            }
        };
        add(surface);
        add(surface1);
        add(surface2);
        add(surface3);
        add(surface4);
        add(surface5);
    }

}
