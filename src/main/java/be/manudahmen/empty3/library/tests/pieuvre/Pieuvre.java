package be.manudahmen.empty3.library.tests.pieuvre;

import be.manudahmen.empty3.*;
import be.manudahmen.empty3.core.Sphere;
import be.manudahmen.empty3.core.nurbs.ParametricCurve;
import be.manudahmen.empty3.core.nurbs.ParametricSurface;
import be.manudahmen.empty3.core.tribase.TRISphere;

import java.awt.*;

/**
 * Created by Win on 15-11-18.
 */
public class Pieuvre extends RepresentableConteneur {
    private Color color;
    private int nbrBras;
    private ParametricSurface bras;
    private ParametricSurface tete;
    private double t;
    double angle;
    Matrix33 matrix22[];


    public Pieuvre(int nbrBras, Color color) {

        this.nbrBras = nbrBras;
        this.color = color;
        matrix22 = new Matrix33[nbrBras];
        for (int i = 0; i < nbrBras; i++) {
            matrix22[i] = new Matrix33(
                    new double[]{Math.sin(angle), Math.cos(angle), 0,
                            -Math.cos(angle), Math.sin(angle), 0,
                            0, 0, 1});
        }

    }

    public void setT(double t) {
        this.t = t;
    }

    class Bras extends ParametricSurface {
        private int noBras;
        private ITexture text;


        public Bras(int noBras, int nbrBras, ITexture text) {
            this.noBras = noBras;
        }

        @Override
        public Point3D calculerPoint3D(double u, double v) {
            Point3D point3D = new Point3D(u * t * Math.sin(2 * Math.PI), Math.cos(2 * Math.PI * v), 0);

            angle = 1.0 * noBras / nbrBras;
            return matrix22[noBras].mult(point3D);
        }

    }

    public Pieuvre() {
        for (int i = 0; i < nbrBras; i++) {
            int noBras = i;
            bras = new Bras(noBras, nbrBras, new ColorTexture(Color.YELLOW));


            add(bras);
        }
        tete = new Sphere(new Axe(Point3D.O0, Point3D.X), 2.0);
        tete.texture(new ColorTexture(Color.RED));

        add(tete);
    }


}
