/***
 * Created by manuel on 28-07-16.
 * <p>
 * Packetage pour le format 3D que je vais créer.
 * Quelques idées de base:
 * - Enregistrer chaque pixel avec une couleur re-définissable. (N°, actions de manipulations de couleurs de pixels.
 * - Redessiner une image sans passer par le Z-Buffer: comme changement de lumière, ou éventuellement (déplacement d'
 * objet.
 */
/***
 * Packetage pour le format 3D que je vais créer.
 * Quelques idées de base:
 *  - Enregistrer chaque pixel avec une couleur re-définissable. (N°, actions de manipulations de couleurs de pixels.
 *  - Redessiner une image sans passer par le Z-Buffer: comme changement de lumière, ou éventuellement (déplacement d'
 *  objet.
 *
 */
package be.manudahmen.empty3.library.tests.formats3d.shape3d;

