package be.manudahmen.empty3.library.tests.TestSphere;

import be.manudahmen.empty3.Point3D;


/***
Global license : 

    Microsoft Public Licence
    
    author Manuel Dahmen <ibiiztera.it@gmail.com>

***/


/**
 *
 * @author Manuel Dahmen <ibiiztera.it@gmail.com>
 */
public class Trajectoires {
    public static Point3D sphere(double longpc, double latpc, double radius)
    {
        return new Point3D (
                Math.cos(longpc * 2 * Math.PI) * Math.cos(Math.PI * latpc),
                Math.cos(longpc * 2 * Math.PI) * Math.sin(Math.PI * latpc),
                    Math.sin(longpc*2*Math.PI)
                
                ).mult(radius);
        
    }
}
